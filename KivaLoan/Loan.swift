//
//  Loan.swift
//  KivaLoan
//
//  Created by Gerson  on 03/09/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import Foundation

class Loan {
    
    var name: String = ""
    var country: String = ""
    var use: String = ""
    var amount: Int = 0
    
}
